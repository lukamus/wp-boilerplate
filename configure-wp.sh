#!/bin/bash

# Variables for arguments
db_name=""
db_user=""
while getopts "d:u:" opt; do
    case $opt in 
    d) db_name=$OPTARG;;
    u) db_user=$OPTARG;;
    esac
done

# Exit if args are missing
if [[ -z "$db_name" || -z "$db_user" ]]; then
    echo "Usage: -d DB_NAME -u DB_USER"
    exit 1
fi

# Initialize git repo and clone latest branch of wordpress
echo 'Installing WordPress'
git init
git submodule add https://github.com/WordPress/WordPress.git wordpress
cd wordpress
branch=`git branch -r | sed 's/^  origin\///' | sort -nr | head -n1`
git checkout $branch
cd ../

# Create new content folder
cp -R ./wordpress/wp-content ./content

# Create uploads folder
mkdir ./content/uploads
chmod 777 ./content/uploads

# Set variables to customise boilerplate files
db_host="localhost"
db_pass=`< /dev/urandom tr -dc a-z0-9 | head -c20`
wp_keys=`wget --quiet -O - https://api.wordpress.org/secret-key/1.1/salt`
wp_table=`< /dev/urandom tr -dc a-z0-9 | head -c6`

# Create config files from boilerplate versions
cp ./.boilerplate/index.php ./index.php
cp ./.boilerplate/.htaccess ./.htaccess
cp ./.boilerplate/.gitignore ./.gitignore
cp ./.boilerplate/wp-config.php ./wp-config.php
sed -e "s/%%DB_HOST%%/$db_host/" \
    -e "s/%%DB_PASSWD%%/$db_pass/" \
    -e "s/%%DB_NAME%%/$db_name/" \
    -e "s/%%DB_USER%%/$db_user/" \
    -e "s/%%WP_KEYS%%/$(echo $wp_keys | sed -e 's/[\/&]/\\&/g')/" \
    -e "s/%%WP_TABLE%%/$wp_table/" \
    < ./.boilerplate/local-config.php > ./local-config.php
cp ./local-config.php ./remote-config.php

# Create database
echo "Configuring database"
vagrant ssh <<EOI
sudo su
mysql --defaults-file=/root/.my.cnf -e "drop database if exists $db_name; \
create database $db_name; \
grant all privileges on $db_name.* to $db_user@localhost identified by '$db_pass'; \
flush privileges;"
exit
EOI
# Setup webserver
#echo "Configuring webserver"
#server_name=$(dirname $PWD)
#server_name=${server_name##*/}
#full_path=${PWD}
#sudo bash <<EOF
#    echo "127.0.0.1    $server_name" >> /etc/hosts
#EOF
#sudo a2ensite $server_name
#sudo service apache2 reload

# Add 'essential' WP plugins
cd content/plugins
git submodule add -f git@bitbucket.org:lukamus/lm-utils.git
#git submodule add -f git://github.com/wp-plugins/rich-contact-widget
git submodule add -f git://github.com/wp-plugins/wordfence
git submodule add -f git://github.com/wp-plugins/wp-livephp
#git submodule add -f git://github.com/wp-plugins/ultimate-maintenance-mode
#git submodule add -f git://github.com/wp-plugins/amr-shortcode-any-widget
git submodule add -f git://github.com/wp-plugins/adminer
#git submodule add -f git://github.com/wp-plugins/backwpup
git submodule add -f git://github.com/wp-plugins/backupwordpress
git submodule add -f git://github.com/wp-plugins/wordpress-importer
git submodule add -f git://github.com/wp-plugins/disable-comments
git submodule add -f git://github.com/wp-plugins/email-address-encoder
#git submodule add -f git://github.com/wp-plugins/wp-migrate-db
git submodule add -f git://github.com/Yoast/wordpress-seo
git submodule add -f git://github.com/Yoast/adminer

# Do clean up
cd ../../
rm -rf .boilerplate
rm ./configure-wp.sh

# Initial Git commit
echo "Committing to local Git repo"
git remote rm origin
git add .
git commit -am "Initial commit"
