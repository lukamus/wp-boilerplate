<?php
// Database constants
define( 'DB_NAME', '%%DB_NAME%%' );
define( 'DB_USER', '%%DB_USER%%' );
define( 'DB_PASSWORD', '%%DB_PASSWD%%' );
define( 'DB_HOST', '%%DB_HOST%%' ); // Probably 'localhost'

// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
%%WP_KEYS%%

// Table prefix
// Change this if you have multiple installs in the same database
$table_prefix  = '%%WP_TABLE%%_';

// JetPack Development Mode
define( 'JETPACK_DEV_DEBUG', true );
