<?php
// Load database info and local development parameters
if ( file_exists( dirname( __FILE__ ) . '/local-config.php' ) ) {
    define( 'WP_LOCAL_DEV', true );
    include( dirname( __FILE__ ) . '/local-config.php' );
} else {
    define( 'WP_LOCAL_DEV', false );
    include( dirname( __FILE__ ) . '/remote-config.php' );
}

// Custom Content Directory
define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/content' );
define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/content' );

// You almost certainly do not want to change these
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

// Language
// Leave blank for American English
define( 'WPLANG', '' );

// Load a Memcached config if we have one
if ( file_exists( dirname( __FILE__ ) . '/memcached.php' ) )
        $memcached_servers = include( dirname( __FILE__ ) . '/memcached.php' );

// This can be used to programatically set the stage when deploying (e.g. production, staging)
//define( 'WP_STAGE', '%%WP_STAGE%%' );
//define( 'STAGING_DOMAIN', '%%WP_STAGING_DOMAIN%%' ); // Does magic in WP Stack to handle staging domain rewriting

// Move site root
define('WP_HOME', 'http://' . $_SERVER['HTTP_HOST']);
define('WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'] . '/wordpress');

// Bootstrap WordPress
if ( !defined( 'ABSPATH' ) )
    define( 'ABSPATH', dirname( __FILE__ ) . '/wordpress/' );
require_once( ABSPATH . 'wp-settings.php' );

// Display errors
@ini_set( 'display_errors', 0 );
define( 'WP_DEBUG_DISPLAY', false );

// Debug mode
if ( 'WP_LOCAL_DEV' ) {
    define( 'SAVEQUERIES', true );
    define( 'WP_DEBUG', true );
    define( 'WP_DEBUG_LOG', true );
}
